import firebase from 'firebase/app';
import "firebase/firestore";
import "firebase/auth";

const config = {
  apiKey: "AIzaSyABneuJJao1KjtxPxvh5nYV87m0qIlX9nM",
  authDomain: "eventbase-webtech.firebaseapp.com",
  databaseURL: "https://eventbase-webtech.firebaseio.com",
  projectId: "eventbase-webtech",
  storageBucket: "eventbase-webtech.appspot.com",
  messagingSenderId: "810797353612",
  appId: "1:810797353612:web:5d87b3463abc337fdd09a9"
  };
firebase.initializeApp(config);

export default firebase;
