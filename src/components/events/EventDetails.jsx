import React, { useState } from 'react'
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import moment from 'moment';

const EventDetails = (props) => {
    // const [url, serUrl] = useState('');
    function copyCodeToClipboard() {
        var el = document.createElement('textarea')
        el.setAttribute('type', 'text')
        el.innerText = window.location.href;
        console.log(el)
        document.body.appendChild(el)
        el.select()
        document.execCommand("copy")
        el.remove()
    }
    const { event, auth } = props;
    console.log(event)
    if(!auth.uid) return <Redirect to="/signin" />
    if( event ) {
        return (
            <div style={{borderRadius: "25px", marginTop: "15vh", marginBottom: "15vh"}} className="container section z-depth-2 white">
            <div className="card z-depth-0">
                <div className="card-content">
                    <span className="card-title text-black ">{event.title}</span>
                    <hr />
                    <p>{ event.desc }</p>
                    <br /> <br />
                    <p>
                        Event Date: <span className="blue-text text-darken-2"> { moment(event.startDate.toDate()).format('L') }</span>
                        <span>&nbsp;&nbsp;</span>
                        Location: <span className="blue-text text-darken-2"> {event.location}</span>
                        <span>&nbsp;&nbsp;</span>
                        Contact Number:<span className="blue-text text-darken-2"> {event.contactNumber}</span>
                        <span>&nbsp;&nbsp;</span>
                        Ref Link:<span className="green-text text-darken-2"> {event.refLink}</span>
                        <span>&nbsp;&nbsp;</span>
                        Cost:<span className="yellow-text text-darken-2"> {event.cost}</span>
                    </p>
                </div>
                <div className="card-action gret lighten-4 grey-text">
                    <div className="right">
                        {/* <span className="black-text text-darken-3">Share Event <i className="material-icons">share</i> : &nbsp;</span>  */}
                        <button className="btn blue darken-3 waves-effect waves-light" onClick={copyCodeToClipboard}>Share
                            <i className="material-icons right">share</i>
                        </button>
                    </div>
                    <div>Posted by {event.authorFName} {event.authorLName}</div>
                    <div>{ moment(event.createdAt.toDate()).calendar() }</div>
                </div>
            </div>
        </div>
        )
    } else {
        return (
                <div className="container center">
                     <div className="progress">
                        <div className="indeterminate"></div>
                    </div>
                </div>
            )
    }

}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const events = state.firestore.data.events;
    const event = events ? events[id] : null;
    return {
        event: event,
        auth: state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'events'}
    ])
)(EventDetails)

