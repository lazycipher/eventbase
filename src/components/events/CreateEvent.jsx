import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createEvent } from '../../store/actions/eventActions';
import { Redirect } from 'react-router-dom';
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";

class CreateEvent extends Component {
    state = {
        title: '',
        desc: '',
        location: '',
        contactNumber: '',
        refLink: '',
        startDate: new Date(),
        cost: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    
    handleDateChange = (date) => {
        this.setState({
            startDate: date
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createEvent(this.state);
        this.props.history.push('/');
    }
    render() {
        const { auth } = this.props;
        if(!auth.uid) return <Redirect to="/signin" />
        return (
        <div style={{marginTop: "5vh"}} className="row">
            <div className="col s12 m8 offset-m2 z-depth-3 white">
                <form onSubmit={this.handleSubmit} className="white">
                    <h5 className="grey-text text-darken-3">Post Event</h5>
                    <div className="input-field">
                        <label htmlFor="title">Title</label>
                        <input type="text" id="title" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="desc">Description</label>
                        <textarea id="desc" className="materialize-textarea" onChange={this.handleChange}></textarea>
                    </div>
                    <div className="input-field">
                        <DatePicker
                            selected={this.state.startDate}
                            onChange={this.handleDateChange}
                        />
                    </div>
                    <div className="input-field">
                        <label htmlFor="cost">Cost/Charges</label>
                        <input type="text" id="cost" onChange={this.handleChange} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="location">Location</label>
                        <input type="text" id="location" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="contactNumber">Contact Number</label>
                        <input type="text" id="contactNumber" onChange={this.handleChange} />
                    </div>
                    <div className="input-field">
                        <label htmlFor="refLink">Ref Link</label>
                        <input type="text" id="refLink" onChange={this.handleChange} />
                    </div>
                    <div className="input-field center">
                        <button className="waves-effect waves-light btn-large blue darken-4 z-depth-2">Post Event <i className="material-icons right">add_circle</i></button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createEvent: (event) => dispatch(createEvent(event))
    }
}

export default connect(mapStateToProps ,mapDispatchToProps)(CreateEvent)
