import React from 'react'
import moment from 'moment';

const EventSummary = ({ event }) => {
  const images = ["https://i.ibb.co/Z69nfN3/Screenshot-from-2020-04-27-06-40-00.png",
                  "https://i.ibb.co/d2R0sdZ/Screenshot-from-2020-04-27-06-39-57.png", 
                  "https://i.ibb.co/z2w0WP1/Screenshot-from-2020-04-27-06-10-09.png",
                  "https://i.ibb.co/JtPyf9N/Screenshot-from-2020-04-27-06-50-37.png", 
                  "https://i.ibb.co/7CG0mxG/Screenshot-from-2020-04-27-06-50-17.png"
                ]

  console.log(event)
  return(
    <div style={{borderRadius: "0 0 25px 25px"}} className="card horizontal z-depth-2 project-summary">
        <div className="card-image">
          <img src={images[Math.floor(Math.random()*images.length)]} height="100%"/>
        </div>
        <div className="card-stacked">
          <div className="card-content grey-text text-darken-3">
              <span className="card-title"><b>Event</b>: {event.title}</span>

              <br /> <br />
              <p >Cost/Charges: <span className="green-text text-darken-2">{event.cost}</span></p>
              <p>Posted by { event.authorFName}</p>
              <p className="grey-text text-darken-2">Posted on { moment(event.createdAt.toDate()).calendar() }</p>
              <p className="grey-text text-darken-2">Event Date: {moment(event.startDate.toDate()).format('L')}</p>
          </div>
          {/* <div class="card-action">
            Ref Link: { event.refLink }
            
         </div> */}
      </div>    
    </div>
  )
}

export default EventSummary;
