import React from 'react';
import './style/footer.css'
const Footer = () => {
  return(
    <footer>
        <div className="footer-copyright">
            <div className="black darken-3">
                <p className="center white-text">
                  <a style={{textDecoration: "underline"}} className="grey-text text-lighten-4" href="https://lazycipher.github.io/" rel="noopener noreferrer" target="_blank">Himanshu Singh</a> | <a style={{textDecoration: "underline"}} className="grey-text text-lighten-4" href="#" rel="noopener noreferrer" target="_blank">Abhimanyu Dwivedi</a>
                </p>
            </div>
        </div>
    </footer>
  )
}

export default Footer;
