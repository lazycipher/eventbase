import React, { Component } from 'react'

export default class About extends Component {
    render() {
        return (
            <div className="container center">
                <div className="row">
                    <div style={{marginRight: '50px'}} className="col s4 m1"></div>
                        <div className="col s4 m3">
                            <div className="card">
                                <div className="card-image">
                                    <img src="https://user-images.githubusercontent.com/17141301/80328004-96b3f900-885b-11ea-95bc-78b74134fec3.jpg" height="300px" />
                                </div>
                                <div className="card-content">
                                    <span className="card-title">Mr. Lalit Sharma</span>
                                    <p>
                                        Mentor, Web Tech,
                                        Galgotias University
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col s4 m3">
                            <div className="card">
                                <div className="card-image">
                                    <img src="https://avatars2.githubusercontent.com/u/17141301" height="300px" />
                                </div>
                                <div className="card-content">
                                    <span className="card-title">Himanshu Singh</span>
                                    <p>
                                        17SCSE101209 <br />
                                        B.Tech CSE
                                        CSE-10
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="col s4 m3">
                            <div className="card">
                                    <div className="card-image">
                                        <img src="https://i.ibb.co/xC7KNvc/Screenshot-from-2020-04-27-07-21-11.png" height="300px" />
                                    </div>
                                    <div className="card-content">
                                        <span className="card-title">Abhimanyu Dwivedi</span>
                                        <p>
                                        17SCSE101395 <br />
                                        B.Tech CSE
                                        CSE-10
                                    </p>
                                    </div>
                            </div>
                    </div>
                </div>
                <div className="row">
                    <div style={{borderRadius: '20px'}} className="container z-depth-3 white">
                        <div style={{padding: '50px', fontSize: '20px'}} className="content">
                        <b>eventBase</b>: It's a project written using ReactJS, Firebase NoSQL Firestore Database, Firebase Serverless Functions(NodeJS), Firebase Authentication, Firebase, etc. 
                        This website aims to provide a portal where usere can post event details and students/users can get benifited from that.
                        This project was build under the <b>Web Technologies Lab PBL Project</b> under the mentorship of <b>Lalit Sharma Sir</b>.
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
